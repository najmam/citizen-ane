A survival-typing web game in which you must bring Radical Peace by rewarding coherent citizens and educating incoherent ones.

## Cheat keys

* `0` gives you a health bonus
* `=` skips the current level

## Who

* music, sfx & pixel art: superjesus
* characters & background: klervix
* code & bugs: najmam & pixjuan

## License

Unless otherwise noted, source code is released under the GNU GPL v3.  
Files under `game/assets/` are released under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
Files under `game/fonts/` and JS libraries include their license information.
