const globalState = {
    lang: "fr",
};

function str(key) {
    const s = window.strings[globalState.lang][key];
    if(s == null) {
        console.debug('unknown string: '+key)
    }
    return s || "UNKNOWN STRING";
}

class LanguageSelectionScene extends Phaser.Scene {
    constructor() { super({ key: "languageSelection" }) }
    preload() {
        this.load.audio('sfx-menu', 'assets/sfx-menu.ogg');
    }
    create() {
        this.sound.add('sfx-menu');
        let txt = this.add.text(10, 10, strings.langSelection, { font: '30px Courier New', fill: '#ffffff' });
        Phaser.Display.Align.In.BottomCenter(txt, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight));
        this.input.keyboard.on('keydown-E', () => {
            this.sound.play('sfx-menu');
            globalState.lang = "en";
            this.scene.start("intro");
        });
        this.input.keyboard.on('keydown-F', () => {
            this.sound.play('sfx-menu');
            globalState.lang = "fr";
            this.scene.start("intro");
        });
    }
}

const backgroundMusic = {
    waitForLoopEndThenSwitchTo: (bgmKey) => {}
};
class BackgroundMusicScene extends Phaser.Scene {
    constructor() { super({ key: "bgm", active: true }) }
    preload() {
        this.load.audio('1-intro-a', 'assets/bgm-1-intro-a.ogg');
    }
    create() {
        this.sound.pauseOnBlur = false;
        this.intro1 = this.sound.add('1-intro-a', { loop: true });
        globalState.curBgm = '1-intro-a';
        this.intro1.play();
    }
}

class IntroScene extends Phaser.Scene {
    constructor() { super({ key: "intro" }) }
    preload() {
    }
    create() {
        this.cameras.main.backgroundColor = Phaser.Display.Color.ValueToColor(cfg.introBackgroundColor);
        let txt = this.add.text(0,0, str("intro"), cfg.introTextStyle);
        Phaser.Display.Align.In.TopCenter(txt, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);

        let pressSpace = this.add.text(0,0, str('pressSpaceToContinue'), cfg.introTextStyleSpace);
        Phaser.Display.Align.In.BottomCenter(pressSpace, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);

        this.input.keyboard.on('keydown-SPACE', () => {
            this.sound.play('sfx-menu');
            this.scene.start("main");
        });
    }
}

class PauseScene extends Phaser.Scene {
    constructor() { super({ key: "pause" }) }
    create() {
        this.sound.play('sfx-menu');
        let txt = this.add.text(10, 10, str("pauseScreen"), { font: '28px Courier', fill: '#ffffff' });
        Phaser.Display.Align.In.BottomCenter(txt, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);
        this.input.keyboard.on("keydown", (ev) => {
            // Escape resumes the game
            if (ev.keyCode === 27) {
                this.sound.play('sfx-menu');
                this.scene.wake("main");
                this.scene.stop();
            }
        });
    }
}

class PressSpaceScene extends Phaser.Scene {
    constructor() { super({ key: "pressSpace" }) }
    init(data) {
        this.text = data.text;
        this.callback = data.callback;
    }
    create() {
        this.cameras.main.backgroundColor = Phaser.Display.Color.ValueToColor(cfg.introBackgroundColor);
        let txtGo = this.add.text(0,0, this.text, cfg.introTextStyle);
        Phaser.Display.Align.In.TopLeft(txtGo, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), -10, -10);

        let pressSpace = this.add.text(0,0, str('pressSpaceToContinue'), cfg.introTextStyleSpace);
        Phaser.Display.Align.In.BottomCenter(pressSpace, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);

        this.input.keyboard.on('keydown-SPACE', () => {
            this.sound.play('sfx-menu');
            this.callback();
            this.scene.stop();
        });
    }
}

class EndgameAndCreditsScene extends Phaser.Scene {
    constructor() { super({ key: "endgameAndCredits" }) }
    create() {
        this.cameras.main.backgroundColor = Phaser.Display.Color.ValueToColor(cfg.endgameBackgroundColor);
        let txtGo = this.add.text(0,0, str('endgameText'), cfg.introTextStyle);
        Phaser.Display.Align.In.TopCenter(txtGo, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), -10, -10);

        let pressSpace = this.add.text(0,0, str('pressSpace'), cfg.introTextStyleSpace);
        Phaser.Display.Align.In.BottomCenter(pressSpace, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);

        this.input.keyboard.on('keydown-SPACE', () => {
            this.sound.play('sfx-menu');
            txtGo.setText(str('creditsText'));
            txtGo.setAlign('center');
            Phaser.Display.Align.In.TopCenter(txtGo, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);
            pressSpace.visible = false;
        });
    }
}

class MainScene extends Phaser.Scene {
    constructor() { super({ key: "main" }) }
    init() {
        this.world = new World();
        this.baseInputState = {
            // simple finite state machine: 'selectAction', then 'selectRule', then 'selectCitizen'. // outdated
            // on bad input, or once the key sequence has been validly input, revert to 'selectAction'. // outdated
            phase: 'selectAction',
            action: null, // 'reward' or 'hurtCitizen'
            citizen: null, // id (integer)
        };
        this.inputState = _.clone(this.baseInputState);
    }
    preload() {
        for(let k of Object.keys(cfg.sfxMap)) {
            this.load.audio('sfx-'+k, `assets/sfx-${cfg.sfxMap[k]}`);
        }
        this.load.image('background', 'assets/background.jpg');
        this.load.image('doctor', 'assets/doctor.png');
        this.load.image('citizen-base', 'assets/citizen-base.png');
        this.load.image('citizen-pants', 'assets/citizen-pants.png');
        this.load.image('citizen-shoes', 'assets/citizen-shoes.png');
        this.load.image('citizen-skin', 'assets/citizen-skin.png');
        this.load.image('citizen-top', 'assets/citizen-top.png');
        this.load.image('citizen-zz-hasCoat1', 'assets/citizen-zz-hasCoat1.png');
        this.load.image('citizen-zz-smokes1', 'assets/citizen-zz-smokes1.png');
        this.load.image('citizen-zz-smokes2', 'assets/citizen-zz-smokes2.png');
        this.load.image('citizen-zz-hat', 'assets/citizen-zz-hat.png');
        this.load.image('key', 'assets/key.png');
        this.load.image('carrotrocket', 'assets/carrotbig.png');
        this.load.image('baseballbat', 'assets/baseballbat.png');
    }
    create() {
        this.input.keyboard.on('keydown', (event) => {
            if(debug) { console.debug("keydown", event.keyCode); }
            // Space and Enter
            if(event.keyCode === 32 || event.keyCode === 13) {
            }
            // Escape pauses the game
            else if(event.keyCode === 27) {
                this.scene.sleep();
                this.scene.run("pause");
            }
            // cheat: '=' (the equal sign) skips the current level
            else if(event.key == '=') {
                this.world.skipLevel();
            }
            // cheat: 0 gives the doctor a bonus of 10 health points
            else if(event.key == '0') {
                this.world.health = Math.min(100, this.world.health+10);
            }
            // (debug) Backspace kills the doctor immediately
            else if (debug && event.keyCode === 8) {
                this.world.kill();
            }
            // (debug) helper for fixing the rule checks
            else if(event.key == '\\') {
                let citizensWithNearestLetters = this.world.level.citizens.filter(c => !c.disabled && _.includes(this.world.curNearestCitizenLetters, c.letter));
                console.log('Citizens with the nearest letters:', citizensWithNearestLetters);
            }
            // letters from A to Z are relayed to the world
            else if (event.keyCode >= 48 && event.keyCode <= 90) {
                this.letterPressed(event.key);
            }
        });

        { // audio game objects
            for(let k of Object.keys(cfg.sfxMap)) {
                this['sfx-'+k] = this.sound.add('sfx-'+k);
            }
            this.sndReward = this['sfx-reward'];
            this.sndHurtDoctor = this['sfx-hurtDoctor'];
            this.sndHurtCitizen = this['sfx-hurtCitizen'];
            this.sndInputFeedback1 = this['sfx-input1'];
            this.sndInputFeedback2 = this['sfx-input2'];
        }

        { // visual game objects
            this.background = this.add.image(0, 0, 'background').setOrigin(0, 0);
            this.levelNumber = this.add.text(970, 5, '', { font: '20px Courier', fill: '#ffffff' });
            this.healthText = this.add.text(5, 5, str('healthLabel'), cfg.healthTextStyle);
            this.healthBar = this.add.graphics();
            this.doctor = this.add.image(cfg.doctorX, cfg.doctorY, 'doctor');
            this.doctor.scale = cfg.doctorScale;
            var doctorHurtTween = this.tweens.add({
                targets: this.doctor,
                x: this.doctor.x - 30,
                ease: 'Circular',
                duration: 80,
                yoyo: true,
                delay: 0,
                paused: true
            });
            this.baseballbat = this.add.image(cfg.baseballBatX, cfg.baseballBatY, 'baseballbat');
            this.baseballbat.angle = cfg.baseballBatAngle;
            this.baseballbat.scale = 0.5;
            this.baseballbat.depth = 90;

            // reward animation: carrot with tween
            const doctorCarrotOffsetX = -30;
            const doctorCarrotOffsetY = 20;
            const destCarrotOffsetX = -10;
            const destCarrotOffsetY = -80;
            this.carrotrocket = this.add.image(cfg.doctorX+doctorCarrotOffsetX, cfg.doctorY+doctorCarrotOffsetY, 'carrotrocket');
            this.carrotrocket.scale = 0.3;
            this.carrotrocket.visible = false;
            this.playRewardAnimation = (citizen) => {
                let srcX = cfg.doctorX + doctorCarrotOffsetX;
                let srcY = cfg.doctorY + doctorCarrotOffsetY;
                let destCoords = citizenCoords(citizen);
                destCoords.x = destCoords.x + destCarrotOffsetX
                destCoords.y = destCoords.y + destCarrotOffsetY
                let dx = destCoords.x - srcX;
                let dy = destCoords.y - srcY;
                this.carrotrocket.x = srcX;
                this.carrotrocket.y = srcY;
                this.carrotrocket.visible = true;
                this.carrotrocket.angle = 270 + Math.atan2(dy,dx) * 180/Math.PI;
                this.tweens.add({
                    targets: this.carrotrocket,
                    y: {
                        value: srcY + dy,
                        ease: 'Back',easeParams:[2],
                        duration: cfg.carrotAnimationDuration,
                        delay: 0,
                    },
                    x: {
                        value: srcX + dx,
                        ease: 'Linear',
                        duration: cfg.carrotAnimationDuration,
                        delay: 0,
                    },
                    angle: {
                        value: 270 - Math.atan2(dy,dx) * 180/Math.PI,
                        ease: 'Linear',
                        duration: cfg.carrotAnimationDuration,
                        delay: 0,
                    },
                    onComplete: () => { this.carrotrocket.visible = false; }
                });
            };

            this.playHurtAnimation = (citizen) => {
                let citizenOffsetX = -50;
                let citizenOffsetY1 = -50;
                let citizenOffsetY2 = 50;

                let citCoords = citizenCoords(citizen);
                let srcX = citCoords.x + citizenOffsetX
                let srcY = citCoords.y + citizenOffsetY1;
                let destX = citCoords.x +  citizenOffsetX;
                let destY = citCoords.y + citizenOffsetY2

                this.baseballbat.x = srcX;
                this.baseballbat.y = srcY;
                this.baseballbat.angle = 30;

                const duration = cfg.carrotAnimationDuration/2;
                this.tweens.add({
                    targets: this.baseballbat,
                    x: {
                        value: destX,
                        ease: 'Linear',
                        duration: duration,
                        delay: 0,
                    },
                    y: {
                        value: destY,
                        ease: 'Linear',
                        duration: duration,
                        delay: 0,
                    },
                    angle: {
                        value: 140,
                        ease: 'Linear',
                        duration: duration,
                        delay: 0,
                    },
                    onComplete: () => {
                        this.baseballbat.x = cfg.baseballBatX;
                        this.baseballbat.y = cfg.baseballBatY;
                        this.baseballbat.angle = cfg.baseballBatAngle;
                    }
                });
            };

            this.instructions = null;
            // this.securityLine = this.add.graphics();
            // this.securityLine.fillStyle(cfg.securityLineColor, cfg.securityLineAlpha);
            // this.securityLine.fillRect(cfg.securityLineX, cfg.citizenTopLaneY + cfg.citizenLaneOffsetY, cfg.securityLineW, cfg.securityLineH);
            // this.securityLine.opacity = 0.1;
            this.citizens = [];
            // this.lanes = [];
            // for(let i = 1; i < cfg.maxNbLanes; i++) {
            //     let lane = this.add.graphics();
            //     lane.fillStyle(cfg.securityLineColor, cfg.securityLineAlpha);
            //     lane.fillRect(cfg.securityLineX+cfg.securityLineW, cfg.citizenTopLaneY + (i+1)*cfg.citizenLaneOffsetY, cfg.screenWidth-cfg.securityLineX, cfg.securityLineW/2);
            //     this.lanes.push(lane);
            // }
            this.rulesBackground = this.add.graphics();
            this.rulesBackground.fillStyle(cfg.rulesBackgroundColor, cfg.rulesBackgroundAlpha);
            this.rulesBackground.fillRect(cfg.rulesX, cfg.rulesY, cfg.rulesW, cfg.rulesH);
            this.rulesBackground.visible = false;

            this.rules = this.add.text(cfg.rulesX+10, cfg.rulesY+35, '', cfg.rulesStyle);
            this.inputFeedbackText = this.add.text(cfg.rulesX, cfg.inputFeedbackTextY, str('inputFeedbackTextDefault'), cfg.inputFeedbackTextStyle);
            this.actionFeedbackText = this.add.text(0, 0, '', cfg.actionFeedbackTextStyle, { align: 'right' });
            var showActionFeedbackText = (text) => {
                console.debug('[action feedback]', text);
                this.actionFeedbackText.setText(text);
                this.time.delayedCall(500, () => {
                    this.actionFeedbackText.setText('');
                });
            };
        }

        { // handle world events
            // at the start of a level, instantiate GOs
            this.world.on("levelReady", () => {
                console.log(`Entering level ${this.world.level.number}`)
                console.log('Rules:', this.world.curRuleLetters.map(x => x.toUpperCase()).join(', '));
                console.log('Current nearest citizens:', this.world.curNearestCitizenLetters.map(x => x.toUpperCase()).join(', '));
                let citizensWithTheseLetters = this.world.level.citizens.filter(c => _.includes(this.world.curNearestCitizenLetters, c.letter));
                console.debug('Citizens with these letters:', citizensWithTheseLetters);

                // rules
                // delay displaying them, so as not to overload the player on their first playthrough
                const ruleDisplayDelay = this.world.level.number === 1 ? cfg.rulesDisplayDelayAtLevelOneStart : cfg.rulesDisplayDelayAtLevelStart;
                this.time.delayedCall(ruleDisplayDelay, () => {
                    this.rules.setText(this.world.level.rulesText);
                    this.rules.setStyle(cfg.rulesStyle);
                    this.rulesBackground.visible = true;
                    this.rules.visible = true;

                    // we instantiate this here so that the font is coherent with the one this.rules uses
                    if(! this.rulesFirstLine) {
                        this.rulesFirstLine = this.add.text(0,0, str('rulesFirstLine'), cfg.rulesStyle);
                        Phaser.Display.Align.In.TopCenter(this.rulesFirstLine, this.add.zone(cfg.screenWidth/2, 0, cfg.screenWidth, 0), 0, 0);
                        this.rulesFirstLine.y = cfg.rulesY;
                    }
                    this.rulesFirstLine.visible = true;
                });
                
                // citizens
                for(let cit of this.citizens) {
                    cit.destroy();
                }
                this.citizens = this.world.level.citizens.map(cit => {
                    let go = this.add.container(-1000, 1000);
                    go.visible = false;
                    go.citizenId = cit.id;
                    go.angle = Phaser.Math.RND.normal()*15;
                    go.y = citizenCoords(cit).y;

                    // the citizen's letter
                    let kx = -40, ky = -15;
                    let keyImg = this.add.image(kx,ky,'key');
                    keyImg.scale = 2;
                    go.add(keyImg);
                    let keyTxt = this.add.text(kx-10,ky-15, cit.letter.toUpperCase(), cfg.citizenLetterTextStyle);
                    go.add(keyTxt);

                    // pants
                    let layerPants = this.add.image(0,0, 'citizen-pants');
                    layerPants.setTint(cfg.citizenPantColors[Phaser.Math.RND.between(0, cfg.citizenPantColors.length-1)]);
                    layerPants.depth = 1+cit.lane;
                    layerPants.scale = cfg.citizenScale;
                    go.add(layerPants);
                    // shoes
                    let layerShoes = this.add.image(0,0, 'citizen-shoes');
                    layerShoes.setTint(cfg.citizenShoeColors[cit.shoeColor]);
                    layerShoes.depth = 1+cit.lane;
                    layerShoes.scale = cfg.citizenScale;
                    go.add(layerShoes);
                    // skin
                    let layerSkin = this.add.image(0,0, 'citizen-skin');
                    layerSkin.setTint(cfg.citizenSkinColors[cit.skinColor]);
                    layerSkin.depth = 1+cit.lane;
                    layerSkin.scale = cfg.citizenScale;
                    go.add(layerSkin);
                    // top
                    let layerTop = this.add.image(0,0, 'citizen-top');
                    layerTop.setTint(cfg.citizenTopColors[Phaser.Math.RND.between(0, cfg.citizenTopColors.length-1)]);
                    layerTop.depth = 1+cit.lane;
                    layerTop.scale = cfg.citizenScale;
                    go.add(layerTop);
                    // base // render it last in order to reinforce the black contours
                    let base = this.add.image(0,0, 'citizen-base');
                    base.depth = 1+cit.lane;
                    base.scale = cfg.citizenScale;
                    go.add(base);
                    // props, if any
                    const sjx = -5, sjy = -5;
                    if(cit.hatColor != 0) {
                        let layerHat = this.add.image(sjx,sjy, 'citizen-zz-hat');
                        layerHat.setTint(cfg.citizenHatColors[cit.hatColor]);
                        layerHat.depth = 1+cit.lane;
                        layerHat.scale = cfg.citizenScale*.73;
                        go.add(layerHat);
                    }
                    if(cit.hasCoat == 1) {
                        let layerCoat = this.add.image(sjx,sjy, 'citizen-zz-hasCoat1');
                        layerCoat.depth = 1+cit.lane;
                        layerCoat.scale = cfg.citizenScale*.73;
                        go.add(layerCoat);
                    }
                    if(cit.smokes != 0) {
                        let layerSmokes = this.add.image(0,0, 'citizen-zz-smokes'+cit.smokes);
                        layerSmokes.depth = 1+cit.lane;
                        layerSmokes.scale = cfg.citizenScale;
                        go.add(layerSmokes);
                    }

                    // make it look like the citizen 'walks' (a quite silly walk)
                    this.tweens.add({
                        targets: go,
                        y: {
                            value: go.y + 3,
                            ease: 'Circular',
                        },
                        angle: {
                            value: go.angle + 15,
                            ease: 'Linear',
                        },
                        duration: 300 + Phaser.Math.RND.normal() * 150,
                        delay: Phaser.Math.RND.normal()*1000,
                        yoyo: true,
                        loop: -1
                    });

                    return go;
                });
            });

            this.world.on("reward", (cit) => {
                this.sndReward.play();
                showActionFeedbackText(str('actionFeedback_wellDone'));
                this.playRewardAnimation(cit);
            });
            this.world.on("hurtCitizen", (cit) => {
                this.sndHurtCitizen.play();
                this.playHurtAnimation(cit);
                showActionFeedbackText(str('actionFeedback_wellDone'));
            });
            this.world.on("hurtDoctor", (reason) => {
                this.sndHurtDoctor.play();
                showActionFeedbackText(str('actionFeedback_'+reason));
                if (reason !== 'badInput' && !doctorHurtTween.isPlaying()) {
                    doctorHurtTween.play();
                }
            });
            this.world.on("citizenCrossedLine", (cit) => {
            });

            this.world.on("levelOver", () => {
                // if it was the last level, go to the credits
                if(this.world.level.number /* 1-indexed */ === window.levelProgression.length) {
                    this.scene.start('endgameAndCredits');
                    return;
                }

                // hide the rules so that they don't show up when starting the next level
                this.rulesBackground.visible = false;
                this.rules.visible = false;

                // show the endtext for this level
                // this['sfx-levelover'].play();
                this.scene.run('pressSpace', {
                    text: this.world.level['endText_'+globalState.lang],
                    callback: () => {
                        this.world.nextLevel();
                    }
                });
            });

            this.world.on("gameOver", () => {
                this['sfx-gameover'].play();
                this.scene.start("gameOver");
            });
        }

        this.world.startLevel();
    }
    update(time, dt) {
        // when a level is over, execute the rest of this function only one last time.
        // this avoid reading this.world while it is setting up for the next level
        if(this.world.levelOver && this.levelOverAtPreviousTick) {
            return; 
        }
        this.levelOverAtPreviousTick = this.world.levelOver;

        this.world.update(time, dt);
        
        // current level
        this.levelNumber.setText(`${str('levelLabel')} ${this.world.level.number}`);

        // input feedback
        if (this.inputState.phase == 'selectAction') {
            this.inputFeedbackText.setText(str('inputFeedbackTextDefault'));
        } else if(this.inputState.phase == 'selectRule') {
            this.inputFeedbackText.setText(str('inputFeedbackTextSelectRule'));
        } else if(this.inputState.phase == 'selectCitizen') {
            if(this.inputState.action === 'reward') {
                this.inputFeedbackText.setText(str('inputFeedbackTextSelectRewardCitizen'));
            } else {
                this.inputFeedbackText.setText(str('inputFeedbackTextSelectHurtCitizen'));
            }
        }
        // action feedback
        const lineHeight = 21;
        Phaser.Display.Align.In.BottomRight(this.actionFeedbackText, this.add.zone(cfg.screenWidth/2, cfg.inputFeedbackTextY/2, cfg.screenWidth, cfg.inputFeedbackTextY), 0, lineHeight);

        // citizens
        for(let go of this.citizens) {
            let cit = this.world.level.citizens[go.citizenId];
            if(cit.disabled) {
                // hide its letter
                go.getAt(0).visible = false
                go.getAt(1).visible = false;
                // hide it once the the reward/hurt animation to complete
                this.time.delayedCall(cfg.carrotAnimationDuration, () => {
                    go.visible = false;
                });
                continue;
            }
            go.x = citizenCoords(cit).x;
            // the base value for go.y is constant. Deviations along y are handled by a tween
            go.visible = (go.x - go.displayWidth/2) <= cfg.screenWidth; // don't render offscreen

            // only show the citizen's letter if it is one of the nearest
            const letterVisible = _.includes(this.world.curNearestCitizenLetters, cit.letter);
            go.getAt(0).visible = letterVisible
            go.getAt(1).visible = letterVisible;
        }

        // health bar
        this.healthBar.clear();
        let healthW = 200;
        let healthX = globalState.lang === "en" ? 85 : 140;
        let healthY = 7;
        let healthH = 21;
        this.healthBar.fillStyle(0x7d7d7d, 1.0);
        this.healthBar.fillRect(healthX, healthY, healthW, healthH);
        this.healthBar.fillStyle(0x23fd45, 1.0);
        this.healthBar.fillRect(healthX, healthY, Math.max(this.world.health, 0)*healthW/100, healthH);
    }
    letterPressed(letter) {
        const reset = () => {
            this.inputState = _.clone(this.baseInputState);
        };

        if(letter == 'g') {
            this.inputState.action = 'reward';
            this.inputState.phase = 'selectCitizen';
            this.sndInputFeedback1.play();
        } else if(letter == 'h') {
            this.inputState.action = 'hurtCitizen';
            this.inputState.phase = 'selectRule';
            this.sndInputFeedback2.play();
        } else {
            // G and H are handled outside of the switch statement so that the player can change their mind
            switch(this.inputState.phase) {
                case 'selectAction':
                    // ignore letters other than G and H
                break;
                case 'selectRule':
                    if(!_.includes(this.world.curRuleLetters, letter)) {
                        reset();
                        this.world.badInput();
                    } else {
                        this.inputState.rule = this.world.level.rules.find(r => r.letter == letter).id;
                        this.inputState.phase = 'selectCitizen';
                        this.sndInputFeedback2.play();
                    }
                break;
                case 'selectCitizen':
                    if(!_.includes(this.world.curNearestCitizenLetters, letter)) {
                        reset();
                        this.world.badInput();
                    } else {
                        this.inputState.citizen = this.world.level.citizens.find(c => !c.disabled && c.letter == letter).id;
                        this.world.doctorAction(this.inputState.action, this.inputState.citizen, this.inputState.rule);
                        reset();
                    }
                break;
                default: throw new Error('unexpected');
            }
        }
    }
}
function citizenCoords(cit) {
    const x0 = cfg.citizenStartX;
    return {
        x: x0 - cit.secondsTraveled*cfg.citizenSpeedX,
        y: cfg.citizenTopLaneY + cit.lane * cfg.citizenLaneOffsetY + 20,
    };
}

class GameOverScene extends Phaser.Scene {
    constructor() { super({ key: "gameOver" }) }
    create() {
        this.sound.get(globalState.curBgm).volume = 0.3;
        this.cameras.main.backgroundColor = Phaser.Display.Color.ValueToColor(cfg.gameoverBackgroundColor);
        let txtGo = this.add.text(0,0, str('gameOverText'), cfg.gameoverTextStyle);
        Phaser.Display.Align.In.Center(txtGo, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);

        let pressSpace = this.add.text(10, 10, str("pressSpaceToRestart"), { font: '28px Courier', fill: '#ffffff' });
        Phaser.Display.Align.In.BottomCenter(pressSpace, this.add.zone(cfg.screenWidth/2, cfg.screenHeight/2, cfg.screenWidth, cfg.screenHeight), 0, -10);
        this.input.keyboard.on('keydown-SPACE', () => {
            this.scene.start("main");
            this.sound.get(globalState.curBgm).volume = 1;
        });
    }
}

const scenes = debug
    ? [MainScene, PauseScene, GameOverScene, BackgroundMusicScene, PressSpaceScene, EndgameAndCreditsScene]
    : [LanguageSelectionScene, IntroScene, PauseScene, MainScene, GameOverScene, BackgroundMusicScene, PressSpaceScene, EndgameAndCreditsScene];

var gameConfig = {
    title: cfg.title,
    url: cfg.url,
    type: Phaser.AUTO,
    parent: "canvas",
    width: cfg.screenWidth,
    height: cfg.screenHeight,
    scene: scenes,
};
var game = new Phaser.Game(gameConfig);

if(debug) {
    setupTweakpane();
}
