const defaultFont = 'Courier';
window.cfg = {
    title: "Citizen Âne",
    url: "https://njmmmr.itch.io/citizen-ane/",
    debug: false,
    screenWidth: 1066,
    screenHeight: 600,

    securityLineX: 243,
    securityLineY: 369,
    securityLineW: 20,
    securityLineH: 290,
    securityLineColor: 0xffffff,
    securityLineAlpha: 0.3,

    doctorX: 150,
    doctorY: 400,
    doctorScale: 1,
    baseballBatX: 141,
    baseballBatY: 528,
    baseballBatAngle: 105,

    citizenSpeedX: 0.07,
    citizenTopLaneY: null,
    citizenLaneOffsetY: 56,
    citizenScale: 0.35,
    citizenStartX: null,
    maxNbLanes: 4,

    healthTextStyle: { fontSize: '24px', fontFamily: defaultFont, fill: '#ffffff' },

    rulesX: 10,
    rulesY: 70,
    rulesW: 1066-20,
    rulesH: 145,
    rulesStyle: { fontSize: '23px', fontFamily: `Russo_One, ${defaultFont}`, fill: '#ffffff', stroke: '#2e3f37', strokeThickness: 8 },
    rulesBackgroundColor: 0x8d9a93,
    rulesBackgroundAlpha: 0.95,
    rulesDisplayDelayAtLevelOneStart: 2000, // milliseconds
    rulesDisplayDelayAtLevelStart: 0, // milliseconds

    introBackgroundColor: 0x000000,
    introTextStyle: { fontSize: '26px', fontFamily: defaultFont, fill: '#ffffff' },
    introTextStyleSpace: { fontSize: '26px', fontFamily: defaultFont, fill: '#ffffff' },

    endgameBackgroundColor: 0x000000,
    creditsBackgroundColor: 0x000000,
    gameoverBackgroundColor: 0x000000,
    gameoverTextStyle: { fontSize: '26px', fontFamily: defaultFont, fill: '#ffffff' },

    citizenLetterTextStyle: { fontSize: '26px', fontFamily: defaultFont, fill: '#000000' },
    citizenProperties: { // number of different values for each property
        hasCoat: 2,
        shoeColor: 2, // light brown + dark brown
        skinColor: 2, // light + dark
        hatColor: 4, // no hat + 3 tints
        smokes: 3, // nothing, cigar, marijuana
    },
    citizenShoeColors: [0x873e23, 0x0000ff], // 0 correspond à "marron", 1 correspond à "bleu"
    citizenSkinColors: [0xffe8d6, 0xa5a58d, 0xddbea9, 0xcb997e, 0xb7b7a4], // 0 correspond à "peaux claires", 1 correspond à "peaux foncées"
    citizenHatColors: [null /* no hat */, 0xff0000, 0x00ff00, 0x0000ff],
    citizenTopColors: [0x94d2bd, 0xca6702, 0xfcd5ce, 0xf1c0e8], // pas utilisé dans les règles
    citizenPantColors: [0xe76f51, 0xffb703, 0x0a9396], // pas utilisé dans les règles

    doctorPenaltyForBadInput: 1,
    doctorPenaltyForBadDiagnosis: 10,
    doctorPenaltyForLineCrossed: 10,
    doctorBonusForLevelOver: 10,

    inputFeedbackTextY: 40,
    inputFeedbackTextStyle: { fontSize: '20px', fontFamily: defaultFont, fill: '#ffffff' },
    actionFeedbackTextStyle: { fontSize: '20px', fontFamily: defaultFont, fill: '#ffffff' },

    sfxMap: {
        // menu: 'menu.ogg', // loaded manually in LanguageSelectionScene
        input1: 'input1.ogg',
        input2: 'input2.ogg',
        reward: 'reward1.ogg',
        // reward2: 'reward2.ogg',
        hurtDoctor: 'hurt1.ogg',
        hurtCitizen: 'hurt2.ogg',
        // hurt3: 'hurt3.wav',
        levelover: 'levelover.ogg',
        gameover: 'gameover.ogg',
    },

    carrotAnimationDuration: 800, // milliseconds
};
window.debug = cfg.debug;

if (debug) {
    // // make it easier to debug levelOver
    // cfg.doctorPenaltyForLineCrossed = 1;
    // cfg.citizenSpeedX = 0.15;
    // cfg.spawnDelayAvg = 200;
}

cfg.citizenTopLaneY = cfg.screenHeight - (cfg.citizenLaneOffsetY + cfg.securityLineW/2)*cfg.maxNbLanes - cfg.securityLineW/2;
cfg.citizenStartX = cfg.screenWidth * 1.1;
cfg.laneDuration = (cfg.citizenStartX - (cfg.securityLineX + cfg.securityLineW*2))/cfg.citizenSpeedX;

function setupTweakpane() {
    const script = document.createElement("script");
    script.setAttribute("src", "tweakpane-3.0.7.min.js");
    document.body.appendChild(script);
    script.addEventListener("load", () => {
        document.querySelector("#tweak-container").className = "visible";

        const pane = new Tweakpane.Pane({ container: document.querySelector("#tweak"), title: "tweak" });
        // pane.addInput(cfg, 'spawnDelayAvg', { min: 300, max: 2000 });
        // pane.addInput(cfg, 'spawnDelayStdDev', { min: 0, max: 5000 });

        // pane.addInput(cfg, 'doctorX', { min: 10, max: cfg.screenWidth });
        // pane.addInput(cfg, 'doctorY', { min: 100, max: cfg.screenHeight });

        // pane.addInput(cfg, 'citizenSpeedX', { min: 0.01, max: 0.24 });
        // pane.addInput(cfg, 'citizenTopLaneY', { min: 0, max: cfg.screenHeight });
        // pane.addInput(cfg, 'citizenLaneOffsetY', { min: 0, max: 400 });

        // pane.addInput(cfg, 'securityLineX', { min: 0, max: cfg.screenWidth });
        // pane.addInput(cfg, 'securityLineY', { min: 0, max: cfg.screenHeight });
        // pane.addInput(cfg, 'securityLineW', { min: 0, max: cfg.screenWidth });
        // pane.addInput(cfg, 'securityLineH', { min: 0, max: cfg.screenHeight });
        // pane.addInput(cfg, 'securityLineColor', { view: "color", alpha: true });
    });
}
