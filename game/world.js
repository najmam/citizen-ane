function randNormal(avg, stdDev) {
    return avg+Phaser.Math.RND.normal()*stdDev;
}

class Citizen {
    // spawn info
    id = 0; // unique id in a level
    letter = null;
    lane = 0;
    birthTime = 0; // time (relative to the beginning of the level) this citizen spawns at
    speed = 1; // relative speed, the norm being 1
    rulesNotRespected = []; // ids of the rules in the level that this citizen doesn't respect

    // updated during play
    uptime = 0;
    secondsTraveled = 0;
}

class Rule {
    id = 0; // unique id in a level
    letter = null;
    forbiddenValues = {};
    text = "";

    isRespectedBy(citizen) {
        for(let k of Object.keys(this.forbiddenValues)) {
            let forbidden = this.forbiddenValues[k];
            if(_.includes(forbidden, citizen[k])) {
                return false;
            }
        }
        return true;
    }

    static ruleForLevel(level) {
        let rule = new Rule();

        return rule;
    }

    static rulesForLevel(level) {
        let res = [];
        let propertiesNotYetUsed = _.shuffle(Object.keys(cfg.citizenProperties));
        for(let i = 0; i < level.nbRules; i++) {
            let rule = new Rule();
            rule.id = i;

            let property = propertiesNotYetUsed.shift();
            let nbValuesForKey = cfg.citizenProperties[property];

            let valueForbidden = Phaser.Math.RND.between(0, nbValuesForKey-1);
            rule.forbiddenValues[property] = [valueForbidden];
            rule.text = str('rule_'+property).replace('??', str('rule_'+property+valueForbidden));

            res.push(rule);
        }
        return res;
    }
}

class Level {
    number = 0;
    rules = [];
    rulesText = "";
    citizens = [];

    static newFromProgression(levelNumber) {
        const levelProperties = ["nbLanes", "nbCitizensPerLane", "nbRules", "endText_fr", "endText_en", "spawnDelayAvg", "spawnDelayStdDev", "firstSpawnDelay"];
        let lvl = new Level();
        lvl.number = levelNumber;
        let ref = window.levelProgression[levelNumber-1];
        for(let k of levelProperties) {
            lvl[k] = ref[k];
        }
        lvl.rules = Rule.rulesForLevel(lvl);
        return lvl;
    }

    spawnCitizens() {
        const reservedLetters = ["g", "h"];
        const lettersAvailable = _.shuffle(Array.from("abcdefijklmnopqrstuvwxyz"));
        // assign letters to rules
        let ruleTexts = []; // and compute the text for all rules
        for(let i = 0; i < this.rules.length; i++) {
            const letter = lettersAvailable.shift();
            this.rules[i].letter = letter;
            ruleTexts.push(`[ ${letter.toUpperCase()} ] ${this.rules[i].text}`);
        }
        this.rulesText = ruleTexts.join("\n");

        let letterPoolForCitizens = []; // levels contain more than 26 citizens, so letters have to be reused

        const nb = this.nbLanes * this.nbCitizensPerLane;
        let t = this.firstSpawnDelay;
        for(let id = 0; id < nb; id++) {
            let cit = new Citizen();
            cit.id = id;
            cit.lane = Phaser.Math.RND.between(0, this.nbLanes-1);
            cit.birthTime = t;

            // assign a letter to this citizen
            if (letterPoolForCitizens.length == 0) { // replenish the letter pool, if needed
                letterPoolForCitizens = _.shuffle(lettersAvailable);
            }
            cit.letter = letterPoolForCitizens.shift();

            // assign random properties to this citizen
            for(let prop of Object.keys(cfg.citizenProperties)) {
                let nbValues = cfg.citizenProperties[prop];
                cit[prop] = Phaser.Math.RND.between(0, nbValues-1);
            }

            // compute which rules this citizen does not follow
            for(let rule of this.rules) {
                if(!rule.isRespectedBy(cit)) {
                    cit.rulesNotRespected.push(rule.id);
                }
            }

            this.citizens.push(cit);
            t += randNormal(this.spawnDelayAvg, this.spawnDelayStdDev);
        }
    }
}

class World extends Phaser.Events.EventEmitter {
    health = 100;
    level = null;
    levelOver = null;
    t0 = null;
    curRuleLetters = null;
    curNearestCitizenLetters = null;

    constructor() {
        super();
        this.level = Level.newFromProgression(1);
    }
    startLevel() {
        this.level.spawnCitizens();
        this.curRuleLetters = this.level.rules.map(r => r.letter);
        this.computeNearestCitizenLetters();
        this.emit("levelReady");
        this.levelOver = false;
    }
    nextLevel() {
        this.level = Level.newFromProgression(this.level.number+1);
        this.startLevel();
    }
    computeNearestCitizenLetters() {
        // sum up the letters for the nearest citizens
        // Here, we are assuming that level.citizens's order, i.e. by "oldest birth date first", is sufficient.
        // This would no longer be the case if citizens were able to cross one another in a lane.
        let firstLetterInLane = {};
        for(let cit of this.level.citizens) {
            if(cit.disabled) {
                continue;
            }
            if(firstLetterInLane[cit.lane] == null) {
                firstLetterInLane[cit.lane] = cit.letter;
            }
        }
        this.curNearestCitizenLetters = Array.from(Object.values(firstLetterInLane));
    }

    kill() {
        this.health = 0;
    }
    skipLevel() {
        for(let cit of this.level.citizens) {
            cit.disabled = true;
        }
    }

    badInput() {
        this.health -= cfg.doctorPenaltyForBadInput;
        this.emit('hurtDoctor', 'badInput');
    }
    doctorAction(playerAction, citizenId, ruleId) {
        let goodAction = true;
        let reasonIfWrongAction = null;
        // precondition: citizenId exists in the level and is not disabled, ruleId (if given) exists in the level
        const cit = this.level.citizens[citizenId];
        const respectsAllRules = cit.rulesNotRespected.length == 0;

        if(respectsAllRules) {
            // if the citizen respects all rules, hurting it is wrong
            if(playerAction === 'hurtCitizen') {
                goodAction = false;
                reasonIfWrongAction = 'badAction';
            }
        } else {
            // if at least one rule is not respected by the citizen, rewarding it is wrong
            if(playerAction === 'reward') {
                goodAction = false;
                reasonIfWrongAction = 'badAction';
            } else if(playerAction === 'hurtCitizen') {
                // but we also need to verify that the reason for which the player hurt the citizen is correct.
                // i.e. the action is correct only if the citizen doesn't respect the given rule
                // i.e. the action is wrong if the citizen respects the given rule
                const respectsThisRule = !_.includes(cit.rulesNotRespected, ruleId);
                if(respectsThisRule) {
                    goodAction = false;
                    reasonIfWrongAction = 'goodActionButBadRule';
                }
            } else {
                throw new Error('programmer error');
            }
        }
        if(goodAction) {
            cit.disabled = true;
            this.emit(playerAction, cit);
        } else {
            this.health -= cfg.doctorPenaltyForBadDiagnosis;
            this.emit('hurtDoctor', reasonIfWrongAction);
        }
    }

    update(time, dt) {
        if(this.t0 == null) {
            this.t0 = time;
        }
        if(this.health <= 0) {
            this.emit("gameOver");
            return;
        }
        if(this.levelOver) {
            return;
        }

        // if there are no more enabled citizens in this level, this level is over
        if(this.level.citizens.filter(c => !c.disabled).length == 0) {
            this.health += cfg.doctorBonusForLevelOver;
            this.health = Math.max(0, Math.min(100, this.health));
            this.emit("rewardDoctor");
            this.emit("levelOver");
            this.levelOver = true;
            return;
        }

        // compute how far each citizen went
        for(let cit of this.level.citizens) {
            if(cit.disabled) {
                continue;
            }
            cit.uptime += dt;
            cit.secondsTraveled = - cit.birthTime + cit.uptime * cit.speed;
            if(cit.secondsTraveled > cfg.laneDuration) {
                this.health -= cfg.doctorPenaltyForLineCrossed;
                this.emit('hurtDoctor', 'securityLineCrossed');
                this.emit("citizenCrossedLine", { id: cit.id });
                cit.disabled = true;
            }
        }

        this.computeNearestCitizenLetters();

        this.health = Math.max(0, Math.min(100, this.health));
    }
}
