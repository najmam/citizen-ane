window.levelProgression = [
  {
    "level": 1,
    "nbLanes": 1,
    "nbCitizensPerLane": 10,
    "nbRules": 1,
    "spawnDelayAvg": 3500,
    "spawnDelayStdDev": 100,
    "firstSpawnDelay": 3000,
    "endText_fr": "Bon d\u00e9but, docteur.\n\nLa rumeur dit qu'une r\u00e8gle diff\u00e9rente s'appliquera demain.\n\nNous n'en savons pas plus, mais avons confiance en votre\nfacult\u00e9 d'adaptation.",
    "endText_en": "That's a good start, doctor.\n\nRumor has it that a different rule will apply tomorrow.\n\nWe don't know much more for now, but we believe in your\nadaptability.\n"
  },
  {
    "level": 2,
    "nbLanes": 1,
    "nbCitizensPerLane": 10,
    "nbRules": 1,
    "spawnDelayAvg": 3000,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "Vous vous en \u00eates bien sorti.\n\nGov est heureux que notre dispositif soit en place.\n\nUne annonce \u00e0 la population sera diffus\u00e9e ce soir.\n\nAttendez-vous \u00e0 un peu plus de monde.",
    "endText_en": "You did well.\n\nGov is pleased by the swift setup of our operation.\n\nAn announcement will be broadcast to the people tonight.\n\nExpect more subjects tomorrow."
  },
  {
    "level": 3,
    "nbLanes": 2,
    "nbCitizensPerLane": 7,
    "nbRules": 1,
    "spawnDelayAvg": 3000,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "Les doigts dans le museau, docteur\u00a0!\n\nJ'esp\u00e8re que ces conditions de travail ne vous malm\u00e8nent pas trop.\n\nQuoiqu'il en soit, l'annonce d'hier soir a \u00e9t\u00e9 largement relay\u00e9e.\n\nCertains de nos citoyens viendront de leur plein gr\u00e9, maintenant.",
    "endText_en": "Piece of carrot cake, doctor!\n\nI hope these work conditions don't wear you out too much.\n\nAnyway, yesterday's announcement has now reached wide.\n\nSome of the citizens will come of their own will, now."
  },
  {
    "level": 4,
    "nbLanes": 2,
    "nbCitizensPerLane": 9,
    "nbRules": 1,
    "spawnDelayAvg": 3000,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "Excellent.\n\nNous c\u00e9l\u00e9brerons votre comp\u00e9tence plus tard.\n\nComme annonc\u00e9 avant-hier, une nouvelle r\u00e8gle s'appliquera\n\u00e0 partir de demain.\n\nNe vous emm\u00ealez pas les sabots.",
    "endText_en": "Excellent.\n\nWe will praise your skills later.\n\nAs announced, a new rule will apply starting tomorrow.\n\nDon't get your hooves crossed."
  },
  {
    "level": 5,
    "nbLanes": 2,
    "nbCitizensPerLane": 9,
    "nbRules": 2,
    "spawnDelayAvg": 3000,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "Remarquable.\n\nQuelle endurance.\n\nCette qualit\u00e9 nous sera utile demain\u00a0: week-end, les citoyens\nne travaillent pas, ils pourront donc venir \u00e0 loisir.\n\nEn avant la coh\u00e9rence, docteur\u00a0!",
    "endText_en": "Remarkable.\n\nHow reliable.\n\nThis quality will be useful tomorrow: weekend day,\ncitizens won't be working, thus they will be able\nto come at leisure.\n\nOnward to coherence, doctor!"
  },
  {
    "level": 6,
    "nbLanes": 3,
    "nbCitizensPerLane": 8,
    "nbRules": 2,
    "spawnDelayAvg": 3000,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "Fantastique.\n\nIl ne devrait pas y avoir plus de monde demain.\n\nToutefois, nous serons dimanche.\n\nLes citoyens ont beau adh\u00e9rer \u00e0 la coh\u00e9rence,\nils seront press\u00e9s d'aller pique-niquer.\n\nJe file. \u00c0 lundi. Bon courage.",
    "endText_en": "Fantastic.\n\nThere shouldn't be much more tomorrow.\n\nHowever, it will be Sunday.\n\nNo matter how much citizens uphold coherence, they will be\nin a hurry to get on with their brunch.\n\nGotta go. See you on Monday. Govspeed."
  },
  {
    "level": 7,
    "nbLanes": 3,
    "nbCitizensPerLane": 8,
    "nbRules": 2,
    "spawnDelayAvg": 1800,
    "spawnDelayStdDev": 400,
    "firstSpawnDelay": 1500,
    "endText_fr": "Ah, fini pour aujourd'hui. Superbe.\n\nOn m'a appel\u00e9 en catastrophe.\nJ'ai du laisser mes enfants sur la paille.\n\nGov a demand\u00e9 aux citoyens les plus r\u00e9ticents\nD'enfin faire preuve de civisme et de prendre part\n\u00e0 l'effort de coh\u00e9rence.\nNous les avons aid\u00e9s \u00e0 venir.\n\nIl faudra ouvrir la quatri\u00e8me file.\n\nReposez-vous bien. \u00c0 demain.",
    "endText_en": "Ah, done for today. Great.\n\nI came on short notice.\nHad to leave the kids before they hit the hay.\n\nGov kindly asked the most reluctant citizens\nto take part in the coherence effort.\nWe helped them with their commute.\n\nWe are going to need the fourth lane.\n\nGet some rest. See you tomorrow."
  },
  {
    "level": 8,
    "nbLanes": 4,
    "nbCitizensPerLane": 8,
    "nbRules": 2,
    "spawnDelayAvg": 2400,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "Pfiou. Que d'\u00e9motions.\n\nGrace \u00e0 vos efforts, presque toute la population\nest d\u00e9sormais coh\u00e9rente.\n\nPour clore cette campagne dans la plus pure coh\u00e9rence,\nGov a \u00e9mis une nouvelle directive bienveillante.\n\nUne derni\u00e8re ligne droite, et nous y serons.\nGov sera fier de vous.",
    "endText_en": "Phew. What a week.\n\nThanks to you, nearly all of our people is coherent.\n\nThis the home stretch. To end this campaign in the purest\ncoherence, Gov issued a new directive.\n\nYou will make Gov proud."
  },
  {
    "level": 9,
    "nbLanes": 4,
    "nbCitizensPerLane": 7,
    "nbRules": 3,
    "spawnDelayAvg": 2000,
    "spawnDelayStdDev": 200,
    "firstSpawnDelay": 1500,
    "endText_fr": "(pas de texte ici, c'est g\u00e9r\u00e9 par endgameText dans strings.js)",
    "endText_en": ""
  }
]
