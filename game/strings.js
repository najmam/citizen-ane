window.strings = {
    langSelection: '[E] for English\n[F] for Français\n',
};

window.strings.en = {
    intro: `Good morning doctor,

Congratulations on your promotion into the ranks of the 
Civilizational Coherence Command for Productivity.
Your hardened eye will be useful in our fight to achieve
Radical Peace as planned by the very charismatic Gov,
our supreme leader.

Every day, we will entrust you with a batch of citizens to
examine and deal with.
We're expecting you to REWARD the coherent and REHABILITATE
the incoherent.

Gov, as always ineffably wise, is counting on each and every
one of us to display an exemplary quality of intervention.
If our trust in you were to fade, we would be compelled
to get ri-  to shorten your career.

You are now free to get to your post.
In Gov we trust.`,
    pressSpaceToRestart: "[SPACE] to restart",
    pressSpaceToContinue: "[SPACE] to continue",
    pressSpace: "[SPACE]",
    pauseScreen: "[ESCAPE] to resume",

    healthLabel: 'TRUST',
    levelLabel: 'Day',

    inputFeedbackTextDefault: "First, [G] Reward or [H] Rehab?", // todo
    inputFeedbackTextSelectRule: '(Rehab) Why', // todo
    inputFeedbackTextSelectRewardCitizen: '(Reward) Which citizen?',
    inputFeedbackTextSelectHurtCitizen: '(Rehab) Which citizen?', // todo

    rulesFirstLine: "INSTRUCTIONS OF THE DAY",
    rule_hasCoat: "Citizens who ?? YELLOW VEST are incoherent.",
    rule_hasCoat0: "do NOT wear a",
    rule_hasCoat1: "wear a",
    rule_shoeColor: "Who wears ?? SHOES is incoherent.",
    rule_shoeColor0: "BROWN",
    rule_shoeColor1: "BLUE",
    rule_skinColor: "No to ?? skins.",
    rule_skinColor0: "LIGHT",
    rule_skinColor1: "DARK",
    rule_hatColor: "?? ? Incoherent.",
    rule_hatColor0: "NO hat",
    rule_hatColor1: "A RED hat",
    rule_hatColor2: "A GREEN hat",
    rule_hatColor3: "A BLUE hat",
    rule_smokes: "??",
    rule_smokes0: "They who do not SMOKE are not in Peace.", // valeurs interdites: 0
    rule_smokes1: "It is forbidden to smoke CIGARS.", // valeurs interdites: 1
    rule_smokes2: "MARIJUANA is evil.", // valeurs interdites: 2

    actionFeedback_wellDone: "Well done!",
    actionFeedback_badInput: "Administrative error.",
    actionFeedback_badAction: "Wrong diagnosis, doctor!",
    actionFeedback_goodActionButBadRule: "Wrong diagnosis, doctor!",
    actionFeedback_securityLineCrossed: "Too late!",

    gameOverText: "You did your best, doctor.\n\nAlas, it wasn't enough.\n\nI hope to never see you again.",
    endgameText: '\nFinally!'+
        '\nWe made it.'+
        '\nCoherence is in full swing.'+
        '\nThank you for your devotion.'+
        '\nYour efforts will be brought up to Gov and praised lavishly.'+
        "\n"+ // afficher la suite dans un autre écran ?
        "\nAlas, I also bring bad news."+
        "\nDuring your time, we noticed that you yourself"+
        "\nwere infringing some of the directives."+
        "\nMany directives."+
        "\nCoherence is the same for everyone, doctor."+
        "\n\nWe are forced to let go of you.",
    creditsText: "\na game by\n\n\n\n\n\n\n\nklervix     najmam     pixjuan     superjesus\n\n\n\n\n\n\n\n\nthx for playing!\nsee you around",

};

window.strings.fr = {
    intro: `Bonjour docteur,

Félicitations pour votre mutation au sein du Commandement de
la Cohérence Civilisationnelle pour la Productivité.
Votre œil aguerri nous sera utile dans notre lutte pour que
règne la Paix Radicale telle que théorisée par le très
charismatique Gov, notre chef suprême.

Chaque jour, nous vous confierons un panel de citoyens à
examiner et traiter.
Nous attendons de vous que vous RÉCOMPENSiez les cohérents
et ÉDUQUiez les incohérents.

Gov, dans son infinie sagesse, compte sur chacun d'entre nous
pour assurer une qualité d’intervention exemplaire.
Si notre confiance en vous venait à se tarir, nous serons 
contraints d’écourter votre carrière.

Vous êtes maintenant libre de vous rendre à votre poste.
Gov soit loué.`,

    pressSpaceToRestart: "[ESPACE] pour recommencer",
    pressSpaceToContinue: "[ESPACE] pour continuer",
    pressSpace: "[ESPACE]",
    pauseScreen: "[ECHAP] pour reprendre",

    healthLabel: 'CONFIANCE',
    levelLabel: 'Jour',

    inputFeedbackTextDefault: "D'abord, [G] Récompenser ou [H] Éduquer ?",
    inputFeedbackTextSelectRule: '(Éduquer) Pour quelle raison, docteur ?',
    inputFeedbackTextSelectRewardCitizen: '(Récompenser) Quel citoyen ?',
    inputFeedbackTextSelectHurtCitizen: '(Éduquer) Quel citoyen ?',

    // lorsqu'un citoyen a la valeur X de la propriété P, alors il enfreint la règle rule_PX.
    // exemple: lorsqu'un citoyen a la valeur 1 de hasCoat, alors il enfreint la règle rule_hasCoat1.
    //          selon index.js, hasCoat=1 correspond à "porte un gilet jaune".
    //          vu que rule_hasCoat1 spécifie que le citoyen ENFREINT la règle,
    //          alors la règle est rule_hasCoat1 = "Les citoyens qui PORTENT un gilet jaune sont incohérents"
    rulesFirstLine: "INSTRUCTIONS DU JOUR",
    rule_hasCoat: "Les citoyens qui ?? GILET JAUNE sont incohérents.",
    rule_hasCoat0: "ne portent pas de",
    rule_hasCoat1: "portent un",
    rule_shoeColor: "Qui porte des souliers ?? est incohérent.",
    rule_shoeColor0: "MARRONS",
    rule_shoeColor1: "BLEUS",
    rule_skinColor: "Non aux peaux ??.",
    rule_skinColor0: "CLAIRES",
    rule_skinColor1: "SOMBRES",
    rule_hatColor: "?? ? Incohérent.",
    rule_hatColor0: "PAS de casquette",
    rule_hatColor1: "Une casquette ROUGE",
    rule_hatColor2: "Une casquette VERTE",
    rule_hatColor3: "Une casquette BLEUE",

    // smokes0 : le citoyen ne fume pas
    // smokes1 : le citoyen fume le cigare
    // smokes2 : le citoyen fume du cannabis
    rule_smokes: "??",
    rule_smokes0: "Qui ne FUME PAS n'est pas en Paix.", // valeurs interdites: 0
    rule_smokes1: "Interdiction de fumer du TABAC.", // valeurs interdites: 1
    rule_smokes2: "Sus aux fumeurs de MARIJUANGA.", // valeurs interdites: 2
    // rule_smokes3: "La Paix Radicale est non-fumeuse.", // valeurs interdites: 1, 2

    actionFeedback_wellDone: "Beau travail !",
    actionFeedback_badInput: "Erreur administrative.",
    actionFeedback_badAction: "Mauvais diagnostic, docteur !",
    actionFeedback_goodActionButBadRule: "Mauvais diagnostic, docteur !",
    actionFeedback_securityLineCrossed: "Trop tard !",

    gameOverText: "Vous avez fait de votre mieux, docteur.\n\nHélas, ce n'est pas suffisant.\n\nJ'espère que nous ne nous reverrons plus.",
    endgameText: '\nEnfin !'+
        '\nGrâce à vous, nous sommes arrivés au bout.'+
        '\nLa cohérence bat son plein.'+
        '\nMerci pour votre dévouement.'+
        '\nVos efforts seront remontés à Gov et copieusement loués.'+
        "\n\n"+ // afficher la suite dans un autre écran ?
        "\nHélas, j'ai aussi une mauvaise nouvelle."+
        "\nAu cours de votre service, nous avons observé que vous\nenfreigniez vous-même certaines des directives."+
        "\nPlusieurs des directives."+
        "\nLa cohérence est la même pour tous, docteur."+
        "\n\nNous sommes contraints de nous séparer de vous.",
    creditsText: "\nun jeu par\n\n\n\n\n\n\n\nklervix     najmam     pixjuan     superjesus\n\n\n\n\n\n\n\n\nmerci d'avoir joué !\nallez a+",
};
