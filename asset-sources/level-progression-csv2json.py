import csv,json
reader = csv.DictReader(open('level-progression.csv', encoding="iso-8859-1"))
res = []
for row in reader:
    obj = {}
    for k,v in row.items():
        if k.startswith('endText_'):
            obj[k] = v.replace('\u0092', "'")
        elif v == "":
            obj[k] = None
        elif k == "rushFactor":
            obj[k] = float(v.replace(",", "."))
        else:
            obj[k] = int(v)
    res.append(obj)
print(json.dumps(res, indent=2))
