libreoffice --headless --convert-to csv "level-progression.ods" --outdir .
cat <(echo -n "window.levelProgression = ") <(python level-progression-csv2json.py) > level-progression.js
rm "level-progression.csv"
